#!/bin/bash

echo -n "What is the name of the *.odt file? "
read odtFileName
echo -n "What is the name of the desired *.md file? "
read mdFileName
pandoc "$odtFileName" -f odt -t markdown-simple_tables-multiline_tables-grid_tables -o "$mdFileName"
