#!/bin/bash

echo -n "What is the name of the *.docx file? "
read docxFileName
echo -n "What is the name of the desired *.md file? "
read mdFileName
pandoc "$docxFileName" -f docx -t markdown-simple_tables-multiline_tables-grid_tables -o "$mdFileName"
