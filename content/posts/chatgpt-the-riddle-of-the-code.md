---
title: "ChatGPT's - The Riddle of the Code"
date: 2022-12-25T12:53:55-05:00
draft: false
summary: "On this Christmas morning, my wife and I were watching the Lord of the Rings: The Fellowship of the Ring movie and I was curious as to what OpenAI's Chat GPT-3 AI would produce if asked to create a poem in the style of Tolkien about a software developer attempting to solve a coding problem. Moreover, the graphic in the post was created by OpenAI's DALL-E-2."
---

![Coding Elf created by https://openai.com/dall-e-2/](/fyi/images/posts/An-elf-in-middle-earth-developing-software.png)

I asked [Chat GPT's AI](https://openai.com/blog/chatgpt/) to create a poem in the style of Tolkien about a software developer attempting to solve a coding problem. In literally a few seconds, here is the output:

### The Riddle of the Code

In a world of ones and zeros,\
Where the code is king and queen,\
There sits a software developer,\
Lost in thought and in between.

He stares at his screen,\
Fingers poised on the keys,\
Trying to solve this problem,\
That has brought him to his knees.

He's tried every line of code,\
Every trick and every turn,\
But still the answer eludes him,\
And his frustration starts to burn.

He takes a deep breath,\
And closes his eyes in thought,\
He knows he must keep trying,\
For the solution must be sought.

And then, in a moment of clarity,\
The answer comes to him at last,\
He lets out a triumphant cheer,\
And the problem is in the past.

So let this be a lesson,\
To all the coders out there,\
Perseverance is key,\
In the world of ones and pairs.
