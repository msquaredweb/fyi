---
title: "Bis 402 Jamstack Sites"
date: 2021-10-22T13:23:29-04:00
draft: false
summary: "I will be teaching a new class for me in the spring of 2022, BIS 402 Programming for E-Business and will be based on the JAMStack."
---

I will be teaching a new class for me in the spring of 2022, BIS 402 Programming for E-Business and will be based on the JAMStack (<https://jamstack.org/>) with Hugo (<https://gohugo.io/>) as the static site generator.

I have two sites for the course, that I am working on. The [first](https://bis402.github.io/) is using Hugo to generate static HTML pages, and the [second](https://bis402jekyll.github.io/) is using Jekyll to generate static pages.

Looking forward to learning more with the students!
