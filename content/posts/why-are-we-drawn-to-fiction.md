---
title: "Why Are We Drawn to Fiction?"
date: 2024-10-19T09:23:19-04:00
draft: false
summary: "The truth can be unsettling, compelling us to question our convictions, confront our biases, and acknowledge harsh realities. To avoid such discomfort, we might seek information that is more comfortable and emotionally appealing. Yet, is it factual?"
---

Have you ever noticed how a fabricated story can sometimes be more appealing than a factual one, even when we are aware of its inauthenticity? This inclination towards fiction, even when we recognize it as such, is a fascinating aspect of human psychology. Yuval Noah Harari, a renowned historian, addresses this phenomenon in his book *Nexus: A Brief History of Information Networks from the Stone Age to AI*.  Harari suggests that fiction's allure stems from its simplicity, affordability, and capacity to evoke pleasant emotions.

![Reality and Fiction](/fyi/images/posts/split-image-messy-reality-clean-fiction.png)

## Simplicity Over Complexity

In a world saturated with information, deciphering the truth can be a daunting task.  Unraveling complex issues often requires significant effort—researching, analyzing, and confronting uncomfortable realities.  Fiction, on the other hand, offers a shortcut.  It presents narratives that are easy to digest, bypassing the intricacies and nuances inherent in real-world situations.

For instance, comprehending the complexities of climate change through scientific data and reports can be challenging.  However, a fictional film depicting a world ravaged by climate-related disasters might be more readily absorbed and could potentially stimulate contemplation on the subject. 

## The Economics of Information

Harari astutely observes that truth can be expensive.  Consider the resources invested in endeavors like investigative journalism, scientific research, or historical analysis.  These pursuits demand substantial time, funding, and expertise.  Conversely, fabricating stories is remarkably cost-effective. Anyone can conjure a narrative or conspiracy theory and disseminate it online in mere seconds.

Consequently, we are inundated with a disproportionate amount of fiction compared to meticulously researched factual information. The ease and low cost of producing fiction contribute to its prevalence.

## The Emotional Appeal of Fiction

The truth can be unsettling, compelling us to question our convictions, confront our biases, and acknowledge harsh realities.  To avoid such discomfort, we might seek refuge in fiction as a form of emotional escapism.  A heartwarming fictional tale about an underdog triumphing against adversity is more likely to inspire optimism than a factual account of poverty statistics.  Similarly, a conspiracy theory attributing the world's problems to a sinister cabal might be more palatable than grappling with the complexity of reality.

Moreover, humans have an innate affinity for narratives.  We are inherently drawn to stories, regardless of their veracity.  Fiction caters to this inclination, providing us with an avenue to experience a spectrum of emotions without the burdens of real-life consequences. 

## Balancing Truth and Order

While acknowledging the appeal of fiction, we must caution against becoming overly reliant on it.  There's a crucial need to cultivate discernment and critical thinking skills to navigate the information landscape effectively.  Harari emphasizes the importance of recognizing that while some fictions, like shared beliefs that underpin social institutions, can be beneficial for maintaining order, they should not eclipse the pursuit of truth.

Ultimately, striking a balance between truth and order is essential for the well-being of individuals and societies. We need to be mindful of the allure of fiction while remaining committed to seeking out and upholding the truth.
