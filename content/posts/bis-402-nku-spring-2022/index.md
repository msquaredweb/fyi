---
title: "Learn  a modern, web development process with BIS 402 - Programming for E-Business"
date: 2021-11-15T08:45:13-05:00
draft: false
summary: "This class will allow you to see how many corporations are developing and publishing both internal and external web content with what has been termed, the JAMStack."
---

Want to gain hands-on insight into a modern, web development process within today's corporate environment? Consider taking [BIS 402 - Programming for E-Business](http://onlinecatalog.nku.edu/preview_course_nopop.php?catoid=9&coid=18181) at [Northern Kentucky University](https://www.nku.edu/) this coming spring semester.

![BIS 402](images/BIS_402_Spring_2022.jpg)

This class will allow you to see how many corporations are developing and publishing both internal and external web content with what has been termed, the JAMStack <https://jamstack.org/>.  While this tech has been around for seven years, from my perspective, its popularity has increased greatly these last few years.

Moreover, in addition to the JAMStack, this Spring 2022's BIS 402 course will be based on Hugo <https://gohugo.io/> as the static site generator. In short, you will be using Git <https://git-scm.com/downloads> and GitHub <https://github.com/> to manage your content, Visual Studio Code <https://code.visualstudio.com/Download> to develop the code and content, Hugo as the static site generator, and GitHub Actions <https://github.com/features/actions> to automate the deployment of your site.

Finally, this is a 7-week course that by the end, you will have built and deployed a functioning and professional-looking web-based blog. In fact, this blog from which you are reading is a blog that I have built and deployed using the Hugo static site generator, Visual Studio Code, Git, and GitLabs <https://about.gitlab.com/>.
