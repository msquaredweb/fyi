---
title: "Linguistics and Ai"
date: 2024-05-10T17:23:53-04:00
draft: false
summary: "AI is not just a buzzword. It is a tool that can be used to solve a wide variety of problems. In this post, I will discuss what I see as the value of Linguistics with AI."
---
![Sanskrit and AI](/fyi/images/posts/ai-sanskrit.png)

I was thinking back to my college days as a junior at Ohio University. It was there and then that I took a Linguistics class for additional credit. I thought, why not? However, I came to find out that the class was a 5-hour credit class during that quarter. Quarter you ask? Yeah. This was when Ohio University and several other universities and colleges were on quarter schedules instead of semesters. Quarters were an 11 week period and then a 12th week of finals.

But 5 credit hours? Given that most classes are worth only 3 credit hours, how much work would you have to do in a 5 credit hour class? Then I thought, "What did I get myself into?"

Long story short, the linguistics course was one of my favorite classes as an undergraduate. A lot of work and study but learning about [phonetics](https://www.britannica.com/science/phonetics), [phonology](https://www.britannica.com/science/phonology), and [morphology](https://www.britannica.com/topic/morphology-linguistics) was extremely interesting.

Given that understanding human language is essential to artificial intelligence, I have been thinking of the value Linguistics. Linguistics provides the foundation for understanding how human language works, including its structure, semantics, and grammar. It seems to me that this knowledge would be essential for training AI systems to comprehend, interpret, and generate human language more effectively. Moreover, I think it would be extremely valuable in fine tuning LLMs to better generate information for specific use cases.

Keep thinking that when you are using OpenAI or Google's Gemini user interface you are working with synthetic brains that are masters at statistical and semantic operations! LLMs are about [natural language processing](https://www.britannica.com/technology/natural-language-processing-computer-science). Aren't Linguists positioned to better understand human language than any other field? Well, I think so. 😊
