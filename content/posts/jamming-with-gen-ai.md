---
title: "Jamming With Gen Ai"
date: 2024-04-04T04:41:12-04:00
draft: false
summary: "I was sitting at my computer at home working on a personal project and I thought I would like to listen to some music but not the usual tunes that Amazon’s AI thinks I want to hear. Mind you, I like the tried and true classic rock songs, but I wanted to hear something different, but still from the same bands I love. Hmmm. What to do?"
---
I was sitting at my computer at home working on a personal project and I thought I would like to listen to some music but not the usual tunes that Amazon's AI thinks I want to hear. Mind you, I like the tried and true classic rock songs, but I wanted to hear something different, but still from the same bands I love. Hmmm. What to do?

OK, ChatGPT (OpenAI), help me out. Below is **me** conversing with the AI.

![Jamming Gen AI](/fyi/images/posts/android_robot_rock_album_cover.png)

**Me: List some classic rock bands from the late 1960's to the mid 1980's.**

> The late 1960s to the mid-1980s was a golden era for classic rock music, with many bands emerging that would go on to define the genre and influence countless artists in the decades to follow. Here are some notable classic rock bands from that period:
>
> 1. **The Beatles (1960-1970)** - Often considered the most influential band of all time, they transitioned from pop sensations to studio innovators, influencing the development of rock and pop music.
>
> 2. **The Rolling Stones** - Emerging in the early 1960s, they became the epitome of the rebellious rock spirit, with a career that spanned decades and produced hits across several eras.
>
> 3. **Led Zeppelin (1968-1980)** - Known for their powerful, guitar-driven blues rock sound, they are often cited as one of the progenitors of heavy metal, though their music spans multiple genres.
>
> 4. **Pink Floyd** - Starting in the mid-1960s, they became pioneers of psychedelic rock and were known for their complex compositions, sonic experimentation, and elaborate live shows.
>
> 5. **The Who** - With their origins in the mid-1960s, The Who were known for their energetic live performances and for creating the rock opera concept with albums like "Tommy."
>
> 6. **Fleetwood Mac** - Originally a British blues band in the late 1960s, they evolved into a pop rock band in the 1970s, producing one of the best-selling albums of all time, "Rumours."
>
> 7. **Queen** - Formed in the early 1970s, they were known for their musical versatility, vocal harmonies, and flamboyant stage presence, leaving a legacy of timeless hits.
>
> 8. **Aerosmith** - Emerging in the early 1970s, they are often referred to as "America's Greatest Rock and Roll Band," known for their blues-based hard rock style.
>
> 9. **AC/DC** - Formed in the early 1970s, this Australian band became one of the most popular and enduring acts in hard rock and heavy metal, known for their high-energy performances.
>
> 10. **The Eagles** - Forming in the early 1970s, they blended rock with country influences, producing some of the most popular songs of the decade, including "Hotel California."
>
> 11. **Bruce Springsteen & The E Street Band** - With his debut in the early 1970s, Springsteen's storytelling songwriting and energetic performances have made him a staple of American rock music.
>
> These bands not only dominated the music charts and radio waves during their heyday but also left a lasting impact on the music industry and culture at large, with their records still being widely listened to and celebrated today.

**Thank you. Now take the above list of bands and list me 1 b-side song from each band.** 
(A little explanation here is due for the younger readers. Back in the day, when you bought a 45 RPM record, there were two songs on it. The A-side was the hit song, and the B-side was a song that was not as popular. Sometimes the B-side was a hidden gem that was as good, if not better than the A-side.)

> Certainly! Listing a B-side song for each of these iconic bands can uncover some hidden gems in their discographies. Here are B-side tracks for each of the bands mentioned, providing a glimpse into the depth of their musical talents beyond their more widely known hits:
>
> 1. **The Beatles**: "Rain" - The B-side to the "Paperback Writer" single, released in 1966, "Rain" is noted for its backward vocal effects and complex rhythm, showcasing the band's experimental phase.
>
> 2. **The Rolling Stones**: "Play With Fire" - Released as the B-side of "The Last Time" in 1965, this song features a sparse arrangement and dark, moody lyrics, a contrast to their more upbeat A-sides.
>
> 3. **Led Zeppelin**: "Hey, Hey, What Can I Do" - The B-side of the "Immigrant Song" single from 1970, this is the band's only non-album track released during their existence.
>
> 4. **Pink Floyd**: "Careful with That Axe, Eugene" - Originally released as the B-side of "Point Me at the Sky" in 1968, this instrumental track is known for its atmospheric build-up and sudden explosive climax.
>
> 5. **The Who**: "I'm a Boy" (B-side version) - The B-side version, found on the flip side of "Ready Steady Who" EP in 1966, differs slightly from the more widely known A-side single version.
>
> 6. **Fleetwood Mac**: "Silver Springs" - Originally the B-side to "Go Your Own Way" in 1976, this song became so popular that it was later included in various compilations and live albums.
>
> 7. **Queen**: "I'm in Love with My Car" - The B-side to "Bohemian Rhapsody" in 1975, showcasing drummer Roger Taylor's vocal talents and passion for automobiles.
>
> 8. **Aerosmith**: "Adam's Apple" - Though more commonly known as a track from the "Toys in the Attic" album (1975), it served as the B-side for the single "Sweet Emotion."
>
> 9. **AC/DC**: "What's Next to the Moon" - A B-side to "Let There Be Rock" in 1977, featuring the band's signature hard rock sound.
>
> 10. **The Eagles**: "The Sad Café" - Although not originally released as a B-side, it was featured as such in certain single releases from the album "The Long Run" (1979), depending on the region.
>
> 11. **Bruce Springsteen & The E Street Band**: "Meeting Across the River" - This track found its way as the B-side to some versions of "Born to Run" in 1975, showcasing a jazzier side of Springsteen's music.
>
> These tracks offer a deeper dive into the archives of these legendary bands, showing off their versatility and the breadth of their musical talents.

**Please put each b-side song in a simple list that I can copy.**

> Sure, here's a simplified list of the B-side songs from each band:
>
> 1. The Beatles - "Rain"
> 2. The Rolling Stones - "Play With Fire"
> 3. Led Zeppelin - "Hey, Hey, What Can I Do"
> 4. Pink Floyd - "Careful with That Axe, Eugene"
> 5. The Who - "I'm a Boy" (B-side version)
> 6. Fleetwood Mac - "Silver Springs"
> 7. Queen - "I'm in Love with My Car"
> 8. Aerosmith - "Adam's Apple"
> 9. AC/DC - "What's Next to the Moon"
> 10. The Eagles - "The Sad Café"
> 11. Bruce Springsteen & The E Street Band - "Meeting Across the River"
>
> Feel free to copy this list for your reference!

From there, I copied each song, pasted it into Amazon Music search, and turned it up.

Please don't judge me but I was only familiar with "Hey, Hey, What Can I Do", "Adam's Apple", "What's Next to the Moon", and "The Sad Café."

With that said, hearing those 'new' old tunes as I worked away sure was nice.
