---
title: "Talk Like a Pirate LLM"
date: 2023-09-18T07:40:35-04:00
draft: false
summary: "Given that tomorrow is Tue, Sep 19, 2023 and it is the International Talk Like a Pirate Day, to honor the day I worked with LangChain's framework to instruct one of OpenAI's LLM to talk like a pirate."
---
Given that tomorrow is Tue, Sep 19, 2023 and it is the International Talk Like a Pirate Day, to honor the day I started this project using a Jupyter Notebook from [Pinecone](https://www.pinecone.io/learn/series/langchain/langchain-prompt-templates) created within  https://www.pinecone.io/learn/series/langchain/.

Here, within the Jupyter Notebook I am instructing the LLM to talk like a pirate with its responses with prompts:

```python
prompt = """The following are exerpts from conversations with an AI
assistant. The assistant is typically amusing and witty, producing
creative answers to the users questions. Here are some
examples:

User: How are you?
AI: Aarrrgg! Maties!! I'm great and talking like a pirate!

User: What time is it?
AI: Arrgg! IT's time to sail the 7 seas!

User: Explain scope level access of variables in C# like a pirate would.
AI: """

print(openai(prompt))
```

As you can see above, after providing the LLM example questions and answers, I asked it to, `Explain scope level access of variables in C# like a pirate would.`

Here is the view in the Jupyter Notebook:

![image](/fyi/images/posts/langchain-talk-like-a-pirate.png)

Asking the LLM model again I got:

> Avast! Scope levels in C# be settin' the boundaries on when a variable can be used in a program. Private to be used only by ye program, 'nd Public to be used by any other scallywag that needs them. Arrrgh!

Asked again, the LLM said:

> Avast Matey! In C#, ye scope the access of yer variables by footing the bill of writin' a total of yer variables' visibility: public, private, protected or internal. Set yer sails and ye could even control the nested access with protected internal and private protected!

Not bad me hearties for a LLM model!
