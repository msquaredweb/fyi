---
title: "Upate Local Repo from Remote Repo"
date: 2022-11-20T15:41:44-05:00
draft: true
summary: "OK, you have a local repo and then create a remote repo on GitHub from that local repo. Then, from one of your different systems you update the remote GitHub repo. You know, when you have your Windows sytem open, remember something you wanted to test, and add that new branch, and commit that branch to the remote repo."
---

OK, you have a local repo and then create a remote repo on GitHub from that local repo. Then, from one of your different systems you update the remote GitHub repo. You know, when you have your Windows sytem open, remember something you wanted to test, and add that new branch, and commit that branch to the remote repo.

Now, you are on your old laptop that now has a Linux distro and you want to pull the new remote branch into your local branch.

```bash
git branch -f remote-branch-name origin/remote-branch-name
git checkout remote-branch-name
```

Note that the -f option will reset `branchname` to `startpoint`, even if `branchname` exists already. Without -f, git branch refuses to change an existing branch.
