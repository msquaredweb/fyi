---
title: "Word to Web in 5 Minutes with Hugo"
date: 2022-07-12T06:29:02-04:00
draft: false
summary: "I was honored to present my lightning talk entitled Word to Web in 5 Minutes with Hugo at Hugo Conf 2022."
---

I was honored to present my lightning talk entitled `Word to Web in 5 Minutes with Hugo` at [Hugo Conf 2022](https://hugoconf.io/).

Here is the link to the talk: [Word to Web in 5 Minutes with Hugo](https://www.youtube.com/watch?v=PC9NZOcCdTI).

Here is the talk's [GitHub repo](https://github.com/m2web/word-to-web). Enjoy!
