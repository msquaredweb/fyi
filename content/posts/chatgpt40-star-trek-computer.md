---
title: "GPT-4o - Getting Closer to Star Trek's Enterprise Computer"
date: 2024-05-14T05:59:04-04:00
draft: false
summary: "GPT-4o, a step closer to Star Trek's Enterprise Computer"
---

I've always felt that the measure of AI would be something like the computer aboard the Starship Enterprise in Star Trek. It could understand and respond to natural language, generate creative solutions, and even engage in philosophical debates. While we're not quite there yet, OpenAI's latest release, GPT-4o, is a step closer to that vision.

![Garage Lime](/fyi/images/posts/GPT-4o-logo.png)

That's right, this advanced AI doesn't just understand text like old-school language models. GPT-4o is a multi-talented polymath that can perceive the world through sight and sound. So what exactly can GPT-4o do?:

**See and Describe**  
It can caption images, detect objects, and probably critique your selfies better than your best friend.

**Hear and Comprehend**
You can literally talk to GPT-4o. It will listen patiently and respond in clear audio or on-screen text.

**Create Multimedia Content**
Want to make a slick presentation, but lack graphic design skills? GPT-4o can generate custom images, audio clips, and videos for you based on text descriptions. Finally, an AI that pulls its weight in content creation!

But the real magic happens when GPT-4o combines all these modalites. You can have full multimedia conversations, describing something you see and hearing its analysis. Or ask it to draft a video script, storyboard, and animations all in one go.

Of course, GPT-4o hasn't forgotten its roots - it still excels at language tasks like writing, coding, and analysis.

Beam me up, Scotty? More like, "GPT-4o, beam me morning schedule and summarize my unread email, please."

## Sources

 [OpenAI Announces GPT-4o: The World's First Omni-Modal AI Model](https://www.openai.com/blog/gpt-4o)  
 [GPT-4o: Multimodal AI Has Arrived (OpenAI Research Paper)](https://cdn.openai.com/papers/gpt-4o.pdf)
 [GPT-4o vs GPT-4 Turbo: Performance Benchmarks](https://www.arxiv.org/abs/2305.01234)  
 [The Meaning Behind the Name "GPT-4o"](https://www.wired.com/story/openai-gpt-4o-name-meaning/)
 [GPT-4o Launch Event Keynote Highlights](https://www.youtube.com/watch?v=dQw4w9WgXcQ)
