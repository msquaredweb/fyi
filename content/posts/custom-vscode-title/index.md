---
title: "Customize VS Code Window Title"
date: 2022-03-21T07:00:37-05:00
draft: false
summary: "How to customize your VS Code editor window title bar text."
---

I use VS Code often in my day-to-day job. Therefore, I customized the look of the title bar. To do this, in VS Code, open your settings by using the following VS Code menu command:
* On Windows/Linux - File > Preferences > Settings
* On macOS - Code > Preferences > Settings

You can also open the Settings editor from the Command Palette (`Ctrl+Shift+P`) with `Preferences: Open Settings`.

In the settings view, search for `window title`. Set the title to your liking. As you can see below, I have it set to the following set: `M Squared (ツ)_/¯ ${dirty}${activeEditorShort}`

![](images/window-title-setting.png)

The above results in the: `M Squared (ツ)_/¯` being displayed along with the file name of the content in the editor. For example, if I have a file named example.json open in the VS Code editor, the following is displayed in the title bar: `M Squared (ツ)_/¯ example.json`.
If I have an unsaved change in the file, the `${dirty}` setting title displays an indicator for when the active editor has unsaved changes: `M Squared (ツ)_/¯ ⚫ example.json`.  

You can also change the VS Code Window Title setting by opening settings.json. Search for this line: `window.title` and make the change:
`"window.title":"M Squared (ツ)_/¯ ${dirty}${activeEditorShort}",`
