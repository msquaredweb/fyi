---
title: "Github Copilot and Commit Messages"
date: 2024-04-24T16:55:15-04:00
draft: false
summary: "I am loving Generative AI concerning code syntax with GitHub Copilot. However, one of the more valuable aspects is the generation of commit comments when submitting the code to source control."
---

![Copilot Can!](/fyi/images/posts/copilot-can.png)

While I am loving Generative AI concerning code syntax, one of the more valuable aspects is the generation of commit comments when submitting the code to source control.

Because it is the code that is compiled, developers give more care and attention to that aspect. They will conduct code reviews, refactor the code, and optimize the code. All that is great! However, when it comes to looking at a code repository's commit history, the semantics of the commit messages are typically less then helpful. 

Thankfully, Microsoft enabled its GitHub Copilot extensions, for both Visual Studio and Visual Studio Code, to assist with commit messages. It looks at the file's previous state, then compares it with the updates, and then creates a comprehensive, detailed commit message.

![AI Generated Commit Message sparkle pen icon in the Git Changes window](/fyi/images/posts/commit-icon.png)

Very cool!
