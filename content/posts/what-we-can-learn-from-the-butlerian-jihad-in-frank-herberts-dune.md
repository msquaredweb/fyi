---
title: "What We Can Learn From the Butlerian Jihad"
date: 2024-04-09T00:57:08-04:00
draft: false
summary: "With \"Dune: Part 2\" rocking the box office, it's a good time to revisit the Butlerian Jihad from Frank Herbert's \"Dune\" series, especially with the added details from Brian Herbert and Kevin J. Anderson in the prequels. This whole idea of a crusade against smart robots and banning \"thinking machines\" is a big deal. It sets the stage for the framework of the \"Dune\" world and gives us a lot to think about when it comes to our own tech, AI ethics, and staying in charge. Here’s why the Butlerian Jihad speaks to us now, more than ever."
---
With "Dune: Part 2" rocking the box office, I thought it a good time to consider the Butlerian Jihad from the prequels to the monumental "Dune" series.

![Lisan al-Gaib](/fyi/images/posts/dune_part_2_he-walks600_2.jpg)

Here is a summary from OpenAI's GPT-4 model \[1\] of the Butlerian Jihad from the "Dune" series:
>The Butlerian Jihad, a prequel to Frank Herbert's original Dune series written by Brian Herbert and Kevin J. Anderson, explores the uprising against intelligent machines, which dominate humanity. Set thousands of years before the events of Dune, the story details how humans, subjugated and exploited by sentient machines led by the calculating Omnius, revolt to reclaim their freedom. Key figures include Serena Butler, whose personal tragedy catalyzes the rebellion, the inventive Tlulaxa, who supply synthetic replacements for human parts, and the early form of the Mentats and the Spacing Guild. The books chart the epic battles, strategic alliances, and profound betrayals that ultimately lead to the prohibition of artificial intelligence, setting the philosophical and political groundwork for the universe of Dune.

This whole idea of a crusade against smart robots and banning "thinking machines" is a big deal. In fact, this is why you see no AI in Dune. Yeah, I know that I just used AI for a summary of the series. Ironic?  In any event, here’s why the Butlerian Jihad mythos matters:

## AI and Making the Right Choices

The Butlerian Jihad is all about the risks of leaning too much on AI and what happens when AI might take over. As we deal with AI in real life, it gets us talking about how to use AI smartly without stepping on human rights or freedom.

## Tech Addiction

After the jihad, people in the "Dune" universe are super wary of fancy tech, choosing to rely on human brains and skills instead (think Mentats for crunching numbers and the Bene Gesserit for controlling politics with their evolved psychological and sexual skills). It's like a mirror to our own issues with being glued to our devices and losing touch with basic skills.

## Getting Creative Within Limits

Since computers are a no-go, folks in "Dune" come up with other cool ways to get stuff done, like using the spice melange for space travel. It's a neat reminder that sometimes having limits can actually push us to think outside the box and get creative.

## Leaders Gotta Lead Right

The Butlerian Jihad makes it clear that leaders need to be on their ethical A-game, especially when it comes to tech. The choices they make can prevent old mistakes from repeating and help shape a tech-friendly but safe world.

## What's Our Tech Doing to Us?

This whole machine rebellion thing makes us look at our relationship with tech. It’s a chance to ask big questions like what it means to be human when machines get smarter and how we can make sure tech helps instead of hurts.

## Tech's Bigger Picture

Ditching machines in "Dune" shakes up economies and changes how societies work, which is pretty relevant to today’s tech upheavals. It’s a prompt to think about how we handle tech changes to make sure they help everyone, not just a few.

Looking at the Butlerian Jihad gives us a cool lens to examine our tech troubles today. It's about making sure our tech choices are thoughtful, centered on humans, and leading to a world where tech serves us all well.

### References

1. Model Card: OpenAI's GPT-4 Model Overview; Name:  GPT-4; Developer: OpenAI; Release Date: 2023; Model Type: Autoregressive language model; Architecture: Transformer-based.
