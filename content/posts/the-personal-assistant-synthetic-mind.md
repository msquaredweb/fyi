---
title: "The Future of Personal Assistance (Is Here)"
date: 2024-03-09T13:44:16-05:00
draft: false
summary: "Imagine you can use your own native language to instruct a large synthetic mind to assist you! Not only that, this brain will keep learning. Moreover, we can tune it and supplement it with our own data. This mind is also a mutlimodal expert in whatever field you need."
---
## How this Thought started

As an OpenAI Plus subscriber, I created a custom ChatGPT to assist me. Here is the model card information:

```yaml
### Model Insights
**Model Name:** ChatGPT  
**Version:** Based on GPT-4 architecture  
**Developer:** OpenAI  
**Release Date:** Prior to April 2023  
**Last Updated:** Information current as of April 2023  
```

More information about the model can be found [here](https://platform.openai.com/docs/models/gpt-4-and-gpt-4-turbo). Nothing fancy here, just OpenAI's GPT-4 model. Trained and fine tuned (or what some would call "lobotomized") to assist with a variety of tasks.

![Synthetic Mind](/fyi/images/posts/synthetic-mind.png)

## My Thought forming

In any event, the output of the one of my GPTs was good; however, in addition to the visible text, my instructions were to provide a link to a downloadable file of the GPT output in markdown (*.md) format, which it was not doing. I started to update the model instructions for GPT to remedy the issue.

While doing that I noted that [Grammarly](https://app.grammarly.com/), which often helps me with my American English grammar and spelling, made suggestions and optimized the text, making it clearer and more concise. Hey Grammerly, how about your UI assisting with AI Prompt writing as a new product offering?

Again, I thought, how amazing this is! You can use your own native language (and I get that I am privileged to be an American English native) to instruct a large synthetic mind to assist you! Not only that, this brain will keep learning. Moreover, we can tune it and supplement it with our own data.

Yeah, I understand that this does not seem like a big deal, but it is. Instead of complex syntax, you can use your native language to instruct a synthetic mind(s) to assist you.

## The Thought's goal

What would it cost to have multiple personal experts to assist you? Personal Trainer, Physician, Financial Advisor? To have access to a multimodal expert in whatever field you need? While scarry to some, I can't help but see the potential. With access to your genetic, financial, medical data, this learning synthetic mind, a.k.a. AI, can help you in all kinds of ways.

Imagine you are not feeling well. You tell your synthetic brain your symptoms, and it can access your medical records, your genetic data, and your current health data. It can then tell you what you need to do as well as assist with setting doctor's appointments, ordering medications, and even setting up a meal plan. Also, it can remind you to take your medications as well as upcoming appointments.

What about a brain that never sleeps that can review your financial data and help you make decisions about your investments, your retirement, and your estate planning? It can tell you that you should be putting more money into your 401k, or that you should be saving more for your children's college education. It can also help you with your taxes and even help you with your budgeting.

## The Accountable thought

Finally, I am reminded of William Gibson's statement "The future is already here – it's just not evenly distributed." I trust that a more equal distribution will be the primary mission of organizations such as OpenAI (Yeah, Sam Altman, I'm loving the easy RAG setup for custom GPTs) to distribute this ability and opportunity more evenly.
